// If running the app using: $ ionic serve --lab,  use this value for the rootUrl:
// var rootUrl = "http://localhost:8100/api";

// If you're running the app on the device using $ ionic run android or $ ionic build ios and xCode build, use this value for the rootUrl:
var rootUrl = "https://ionicapplication.herokuapp.com/api";  

// Change this ID with your Google Project ID.
var GOOGLE_SENDER_ID = '957003541641';
