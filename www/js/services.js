angular.module('starter.services', ['underscore'])

.factory('LoginServiceFactory', function($q, $http, _) {

  var users = [];

  return {
    loginUser: function(name, pw) {
      var deferred = $q.defer();

      // TODO Check the hashed password for the login.
      $http.get(rootUrl + '/users').success(function(data) {
        var user = _.find(data, function (user) {
          return name == user.username && pw == user.password
        });

        if(user) {
          deferred.resolve('Welcome ' + user.username + '!');
        } else {
          deferred.reject('Wrong credentials.');
        }
        return deferred.promise;
      });
      deferred.promise.success = function(fn) {
        deferred.promise.then(fn);
        return deferred.promise;
      }
      deferred.promise.error = function(fn) {
        deferred.promise.then(null, fn);
        return deferred.promise;
      }
      return deferred.promise;
    },
    getUserDetails: function (loggedUser) {
      return $http.get(rootUrl + '/users/' + loggedUser).success(function(data) {
        return _.extend(data[0], loggedUser);
      });
    }
  };
})

.factory('PatientsFactory', function($http, _) {

  return {
    all: function(){
      return $http.get(rootUrl + '/patients').then(function(patients){
        return patients;
      });
    },
    getPatient: function(patientId) {
      return $http.get(rootUrl + '/patients').then(function(patients){
        return _.find(patients.data, function(patient) {
          return patient._id == patientId;
        });
      });
    },
    remove: function(patient) {
      return $http.delete(rootUrl + '/patients/' + patient.cpr).success(function(data) { });
    },
  };
})

.factory('PatientNewsFactory', function($http, _) {

  return {
    all: function() {
      return $http.get(rootUrl + '/newsList').then(function(patientsNews){
        return patientsNews;
      });
    },
    getPatient: function(patientId) {
      return $http.get(rootUrl + '/newsList').then(function(patientsNews){
        return _.find(patientsNews.data, function(patientNews) {
          return patientNews._id == patientId;
        });
      });
    },
    remove: function(patient) {
      return $http.delete(rootUrl + '/news/' + patient._id).success(function(data) { });
    }
  };
})

.factory('DeviceFactory', function($http) {

  return {
    all: function() {
      return $http.get(rootUrl + '/devices').success(function(data) {
        return data;
      });
    }
  };
});
