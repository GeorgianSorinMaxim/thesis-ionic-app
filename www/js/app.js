var regidiOS = "";
var regidAndroid = "";

angular.module('starter', ['ionic', 'ionic.service.core', 'underscore', 'starter.controllers', 'starter.services', 'ngCordova'])

  .run(function($cordovaPush, $ionicPlatform, $ionicPopup, $rootScope, $state) {

    var androidConfig = {
      "senderID": GOOGLE_SENDER_ID,
    };

    var iosConfig = {
      "badge": true,
      "sound": true,
      "alert": true,
    };

    $ionicPlatform.ready(function() {

      if(window.Connection) {
        if(navigator.connection.type == Connection.NONE) {
            $ionicPopup.confirm({
              title: 'No Internet Connection',
              content: 'Sorry, no Internet connectivity detected. Please reconnect and try again.',
              buttons: [
                {
                    text: 'Exit'
                }]
            })
            .then(function(result) {
                if(!result) {
                    ionic.Platform.exitApp();
                }
            });
        }
      }

      // iOS Push Register
      // var push = new Ionic.Push({
      //   "debug": true
      // });

      // push.register(function(token) {
      //   regidiOS = token.token;
      //   // TODO save the token on the server
      // });

      // pushNotification = window.plugins.pushNotification;
      // pushNotification.register(
      //   tokenHandler,
      //   errorHandler,
      //   {
      //       "badge":"true",
      //       "sound":"true",
      //       "alert":"true",
      //       "ecb":"onNotificationAPN"
      //   }
      // );

      function tokenHandler (result) {
          regidiOS = result;
          console.log('device token = ' + result);
      }

      function successHandler (result) {
          pushNotificationRes = result;
          console.log('result = ' + result);
      }

      function errorHandler (error) {
          console.log('result = ' + result);
      }


      // Android Push GCM Register
      // https://github.com/phonegap/phonegap-plugin-push/blob/master/docs/EXAMPLES.md
      var deferred;

      function registerWithGCMServer() {
          deferred = $.Deferred();

          window.plugins.pushNotification.register(
              function() {
              },
              function() {
                  deferred.reject('Error registering.');
              }, {
                  senderID: GOOGLE_SENDER_ID,
                  ecb: 'window.onAndroidNotification'
              });

          setTimeout(function() {
              if(deferred.state() === 'pending') {
                  deferred.reject('Error registering (timeout).');
              }
          }, 10000);

          return deferred.promise();
      }

      window.onAndroidNotification = function(e) {
          switch (e.event) {
            case 'registered':
              deferred.resolve(e.regid);
            break;

            case 'message':
              if (e.foreground) {
                onMessageRecived.call(null, e.message);
                // alert("Foreground", JSON.stringify(e));
              }
              else {
                if(e.coldstart) {
                  // alert("ColdStart", JSON.stringify(e));
                  $state.go('tab.account');
                }
              }
            break;
          }

          // TODO ColdStart
          // if(e.event == 'registered') {
          //     deferred.resolve(e.regid);
          // } else if(e.event == 'message') {
          //     if (e.event == 'coldstart') {
          //         alert(JSON.stringify(e));
          //         $state.go('tab.account');
          //         var data = JSON.parse(state.data);
          //         alert(data);
          //         window.location.href = 'tab.account';
          //     } else {
          //     // alert(JSON.stringify(e));
          //     onMessageRecived.call(null, e.message);
          //   }
          // }
      };

      registerWithGCMServer()
          .then(function(deviceToken) {
              console.log('Device registered and its token is ' + deviceToken);
              regidAndroid = deviceToken;
          })
          .fail(function(e) {
              console.error(e);
          });

      function onMessageRecived(message) {
          // console.log('Push message received: ' + message);
      }
    });
  })

  .config(function($stateProvider, $urlRouterProvider, $ionicConfigProvider) {

    $ionicConfigProvider.tabs.position('bottom');

    $stateProvider
    .state('login', {
        url: '/login',
        templateUrl: 'templates/login.html',
        controller: 'LoginCtrl'
    })

    .state('tab', {
      url: '/tab',
      abstract: true,
      templateUrl: 'templates/tabs.html'
    })

    .state('tab.account', {
      url: '/account',
      cache: false,
      views: {
        'tab-account': {
          templateUrl: 'templates/tab-account.html',
          controller: 'AccountCtrl'
        }
      }
    })

    .state('tab.news', {
      url: '/news',
      cache: false,
      views: {
        'tab-news': {
          templateUrl: 'templates/tab-news.html',
          controller: 'NewsCtrl'
        }
      }
    })
    .state('tab.news-detail', {
      url: '/news/:patientId',
      cache: false,
      views: {
        'tab-news': {
          templateUrl: 'templates/news-detail.html',
          controller: 'NewsDetailCtrl'
        }
      }
    })

    .state('tab.patient', {
        url: '/patients',
        cache: false,
        views: {
          'tab-patients': {
            templateUrl: 'templates/tab-patients.html',
            controller: 'PatientsCtrl'
          }
        }
      })
    .state('tab.patient-detail', {
      url: '/patients/:patientId',
      cache: false,
      views: {
        'tab-patients': {
          templateUrl: 'templates/patient-detail.html',
          controller: 'PatientDetailCtrl'
        }
      }
    });

    $urlRouterProvider
    .otherwise('/login');

  });
