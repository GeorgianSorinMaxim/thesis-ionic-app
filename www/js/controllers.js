angular.module('starter.controllers', ['ngCordova', 'ngResource'])
  .factory('Device', function($resource) {
    return $resource(rootUrl + '/devices/:id');
  })

  .controller('LoginCtrl', function($scope, LoginServiceFactory, $ionicLoading, $ionicPopup, $state, $rootScope) {
    $scope.data = {};
    $rootScope.loggeduser = {};

    $scope.login = function() {
        $ionicLoading.show({
          template: '<p>Authenticating...</p><ion-spinner></ion-spinner>'
        });

        if(!$scope.data.username && !$scope.data.password) {
            popup();
        }

        LoginServiceFactory.loginUser($scope.data.username, $scope.data.password).success(function() {
            $rootScope.loggeduser.username = $scope.data.username;
            $ionicLoading.hide();
            $state.go('tab.news');
            $scope.data.username = '';
            $scope.data.password = '';
        }).error(function() {
            popup();
        });

        function popup() {
          $ionicLoading.hide();
          $ionicPopup.alert({
            title: 'Login failed!',
            template: 'Please check your credentials!'
          });
        }
    }
  })

  .controller('AccountCtrl', function($scope, LoginServiceFactory, $ionicLoading, $rootScope, $http, $ionicPlatform, $cordovaDevice, $state) {
    $ionicLoading.show({
      template: '<p>Loading Data</p><ion-spinner></ion-spinner>'
    });

    $scope.manufacturer = $cordovaDevice.getDevice().manufacturer;
    $scope.model = $cordovaDevice.getDevice().model;
    $scope.platform = $cordovaDevice.getDevice().platform;
    $scope.uuid = $cordovaDevice.getDevice().uuid;

    $scope.devidiOS = regidiOS;
    $scope.devidAndroid = regidAndroid;

    $scope.deviceInfo = $cordovaDevice.getDevice();

    $scope.$on('$ionicView.loaded', function() {
        LoginServiceFactory.getUserDetails($rootScope.loggeduser.username).then(function(User) {
          $scope.user = User.data[0];
          $ionicLoading.hide();
        });
    });

    $scope.logout = function() {
      $state.go('login');
      $rootScope.loggedUser = [];
      $scope.user = [];
    };
  })

  .controller('NewsCtrl', function($scope, $http, $ionicPlatform, $timeout, $ionicPopup, $ionicActionSheet, $state, $ionicLoading, $cordovaDevice, $cordovaNetwork, $rootScope, PatientNewsFactory, Device, DeviceFactory) {

    $scope.$on('$ionicView.loaded', function() {
        PatientNewsFactory.all().then(function(PatientNews) {
          $rootScope.patientsnews = PatientNews.data;
          $ionicLoading.hide();
        });
        $ionicPlatform.ready(function() {

            // Save the Android GCM registration ID in the DB.
            DeviceFactory.all().then(function(data) {
              // console.log(data, regidAndroid);
              var counter = 0;

              for (var i = 0; i < data.length; i++) {
                if (data[i].uuid == $scope.uuid && data[i].regid === $scope.devidAndroid) {
                  counter++;
                }
              }
              if (counter === 0 || data.length === 0) {
                  $scope.entry = new Device();
                  $scope.entry.regid = regidAndroid;
                  $scope.entry.uuid = $cordovaDevice.getDevice().uuid;
                  Device.save($scope.entry, function() { });
                }
            });

        });

        $ionicLoading.show({
          template: '<p>Loading Data</p><ion-spinner></ion-spinner>'
        });

    });

    $scope.doRefresh = function() {
      $http.get(rootUrl + '/newsList')
       .success(function(newItems) {
          $rootScope.patientsnews = newItems;
          $state.go($state.current, {}, {reload: true});
       })
       .finally(function() {
         $rootScope.$broadcast('scroll.refreshComplete');
       });
    };

    $scope.showSort = function() {
      var hideSheet = $ionicActionSheet.show({
        buttons: [
          { text: 'Alphabetical' },
          { text: 'NEWS Score' }
        ],
        titleText: 'Sort the list',
        buttonClicked: function(index) {
          if(index === 0) {
            $scope.sortby = "firstname";
          } else if (index === 1) {
            $scope.sortby = "score";
          }
          return true;
        }
      });

      $timeout(function() {
        hideSheet();
      }, 5000);
    };

    $scope.remove = function(patient) {
      $rootScope.patientsnews = PatientNewsFactory.remove(patient);
      $state.go($state.current, {}, {reload: true});
    };

  })

  .controller('NewsDetailCtrl', function($scope, $rootScope, $stateParams, $timeout, $state, $ionicLoading, PatientNewsFactory) {
    $scope.$on('$ionicView.loaded', function() {
      $ionicLoading.show({
        template: '<p>Loading Data</p><ion-spinner></ion-spinner>'
      });
      PatientNewsFactory.getPatient($stateParams.patientId).then(function(Patients) {
        $rootScope.patientn = Patients;
        $ionicLoading.hide();
      });
    });
  })


  .controller('PatientsCtrl', function($scope, $http, $ionicActionSheet, $rootScope, $state, $ionicLoading, $timeout, PatientsFactory) {

    $scope.$on('$ionicView.loaded', function() {
      $ionicLoading.show({
        template: '<p>Loading Data</p><ion-spinner></ion-spinner>'
      });
      PatientsFactory.all().then(function(Patients) {
        $rootScope.patients = Patients.data;
        $ionicLoading.hide();
      });
    });

    $scope.doRefresh = function() {
      PatientsFactory.all().then(function(Patients) {
        $rootScope.patients = Patients.data;
        $state.go($state.current, {}, {reload: true});
      });
      $rootScope.$broadcast('scroll.refreshComplete');
    };

    $scope.showSort = function() {
      var hideSheet = $ionicActionSheet.show({
        buttons: [
          { text: 'Alphabetical' },
          { text: 'Triage Score' }
        ],
        titleText: 'Sort the list',
        buttonClicked: function(index) {
          if(index === 0) {
            $scope.sortby = "firstname";
          } else if (index === 1) {
            $scope.sortby = "triage";
          }
          return true;
        }
      });

      $timeout(function() {
        hideSheet();
      }, 5000);
    };

    $scope.remove = function(patient) {
      $rootScope.patients = PatientsFactory.remove(patient);
      $state.go($state.current, {}, {reload: true});
    };
  })

  .controller('PatientDetailCtrl', function($scope, $rootScope, $state, $stateParams, $ionicNavBarDelegate, $ionicLoading, $timeout, PatientsFactory) {
    $scope.$on('$ionicView.loaded', function() {
      $ionicLoading.show({
        template: '<p>Loading Data</p><ion-spinner></ion-spinner>'
      });
      PatientsFactory.getPatient($stateParams.patientId).then(function(Patients) {
        $rootScope.patient = Patients;
        $ionicLoading.hide();
      });
    });

});
