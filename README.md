**You are able to deploy the app and skip the setup of the database and server if you're using the default database and deployed server.**

## If you haven't setup the server, do that before installing the app. See here how to setup the server: https://bitbucket.org/GeorgianSorinMaxim/thesis-nodejs-server

## **Quick install** (using the default MongoDB hosted on [mlab.com](Link URL) and the default deployed Node.js server that can be found here: https://ionicapplication.herokuapp.com)

 - Install Node.js on your machine using this steps: https://nodejs.org/en/.

 - Install Ionic using the following command:
```
#!javascript

    $ npm install -g cordova ionic

```

 - Download the repoository containing the Ionic application from BitBucket:
```
#!javascript

    $ git clone https://GeorgianSorinMaxim@bitbucket.org/GeorgianSorinMaxim/thesis-ionic-app.git

```

- Deploy on an Android device, by running the following commands in the terminal after opening the root of your Ionic folder in your terminal:
```
#!javascript
    $ npm install
    $ npm install ngCordova
    $ npm install phonegap
    $ cordova platform add android
    $ cordova plugin add https://github.com/phonegap-build/PushPlugin.git
    $ cordova plugin add cordova-plugin-device
    $ cordova plugin add cordova-plugin-console
    $ cordova plugin add cordova-plugin-whitelist
    $ cordova plugin add cordova-plugin-splashscreen
    $ cordova plugin add cordova-plugin-statusbar
    $ cordova plugin add phonegap-plugin-push --variable SENDER_ID="957003541641"
    $ ionic run android
    
```
- You might be required to enable the Developer options on your device (see here how to do it: http://www.greenbot.com/article/2457986/how-to-enable-developer-options-on-your-android-phone-or-tablet.html). You can check the status of your device using the following command in the terminal when having a device plugged into an USB port:
```
#!javascript
    $ adb devices

```

- Open the app on the Andriod device and login using use the **username: Admin** and the **password: admin**.

- Open the server that can be found at the following URL: https://ionicapplication.herokuapp.com
  - Send a notification using the "Send Message" tab by inputting a title and a body message and pressing send.
  - Observe the notification received on the Android device (and on the paired smartwatch, if any).





## **Custom install** (with newly created MongoDB and deployed Node.js server)

 - Install Node.js on your machine using this steps: https://nodejs.org/en/

 - Install Ionic using the following command:
```
#!javascript

    $ npm install -g cordova ionic

```

 - Download the repoository containing the Ionic application from BitBucket:
```
#!javascript

    $ git clone https://GeorgianSorinMaxim@bitbucket.org/GeorgianSorinMaxim/thesis-ionic-app.git

```


### How to deploy the Ionic app on the connected Android device:
 - Run the following commands in the terminal after opening the root of your Ionic folder, with the Sender_ID (Proejct Number) created in the Google Developer Console when setting up the Node.js server:

```
#!javascript
		$ android -v (and choose Android SDK 23, Android-Support-Library, Google Play Services, Google Repository)
    $ npm install
    $ cordova platform add android
    $ cordova plugin add cordova-plugin-device
    $ cordova plugin add cordova-plugin-console
    $ cordova plugin add cordova-plugin-whitelist
    $ cordova plugin add cordova-plugin-splashscreen
    $ cordova plugin add cordova-plugin-statusbar
    $ cordova plugin add phonegap-plugin-push --variable SENDER_ID="YOUR_GCM_PROJECT_NUMBER"

```


- If you're using the default MongoDB database, the default Node.js server and the default Google Cloud Messaging project, you do not need to change the values of the rootUrl and SenderID variables, so skip the next step:

- If you're creating a new Google Project in the Google Developer Dasbhoard (https://developers.google.com), follow this next step:

- You need to add a proxy with the url of your server you created earlier for the Node.js server, in the ionic.project file:

```
#!javascript

   "proxies": [
    {
      "path": "/api",
      "proxyUrl": "YOUR_URL_GOES_HERE/api"
    }
  ]
```


- Deploy on an Android device, by running the following command in the terminal after opening the root of your Ionic folder:
- Before running the app on the device, make sure that you're using the value of your server for the rootUrl variable in the www/js/config.js file: 
```
#!javascript

   var rootUrl = "YOUR_URL_GOES_HERE/api";
```

- If you're using the the test server with the default MongoDB database use this value for the rootUrl variable: 
```
#!javascript

    var rootUrl = "https://ionicapplication.herokuapp.com/api";
```
 
- Deploy on the connected Android device:
```
#!javascript

    $ ionic run android
    

```

- You might be required to enable the Developer options on your device (see here how to do it: http://www.greenbot.com/article/2457986/how-to-enable-developer-options-on-your-android-phone-or-tablet.html). You can check the status of your device using the following command in the terminal when having a device plugged into an USB port:
```
#!javascript
    $ adb devices

```

- If you're using the test server with the default MongoDB database, when logging in the app, use the **username: Admin** and the **password: admin**.
- If you're using a newly created database, you must create a new user in by going to the server URL and navigate to the Register User page, register a new user and remeber the chose username and password.


### Deploy on an Apple device, by running the following command in the terminal after opening the root of your Ionic folder:
- Before running the app on the device, make sure that you're using the value of your server for the rootUrl variable in the www/js/config.js file:
```
#!javascript
    var rootUrl = "YOUR_URL_GOES_HERE/api";
```
If you're using the the test server with the default MongoDB database use this value for the rootUrl variable: 

```
#!javascript

    var rootUrl = "https://ionicapplication.herokuapp.com/api";
```
  

```
#!javascript

    $ ionic build ios

```
- Open xCode and open the project found in the platforms/ios/ folder. Run the project on your connected iOS device.

- **!! Note that when deploying locally you cannot receive push notifications!!** Delpoy locally to view the UI of the app, by running the following command in the terminal after opening the root of your Ionic folder:
- For the best performance when running the app in the browser, use Google Chrome. Add and enable the following extension in Google Chrome. https://chrome.google.com/webstore/detail/ripple-emulator-beta/geelfhphabnejjhdalkjhgipohgpdnoc/related?hl=en
- Before running the app locally, make sure that you're using this value for the rootUrl variable in the www/js/config.js file: var rootUrl = "http://localhost:8100/api";
- Consider using Ripple, a Chrome extension (a browser based html5 mobile application development and testing tool).

```
#!javascript

    $ ionic serve --lab

```



## **Debugging on the device**

- Use the device Chrome Developer Tools by navingating to the URL in your browser: chrome://inspect/#devices 

- View the console logs while the app is running on the device,  by running the following command in the terminal after opening the root of your Ionic folder:

```
#!javascript

    $ ionic run android -consolelogs
    $ c

```
    OR
```
#!javascript

   $ adb shell
   $ logcat

```